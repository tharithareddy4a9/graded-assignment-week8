create database library;
use library;

create table Books(
id int(20) auto_increment primary key,
Title varchar(60),
Authorname varchar(40),
Genre varchar(30),
Price float);

insert into Books values(1,"Little Book","Subha Prabhu"," Novel, High fantasy","3000");
insert into Books values(2,"Dream of Electric"," Philip","Novel, Electrics","265");
insert into Books values(3,"Everything I Never Told You","Celeste Ng","Novel, Fiction","5000");
insert into Books values(4,"The Guide","R.k.Narayan","Novel, action","800");
insert into Books values(5,"Private Life Of Indian prince","mulk Raj","Novel, History","4000");
insert into Books values(6,"Train to Pakistan","Sing"," Novel, History novel","3000");
insert into Books values(7,"The White Tiger","Aravind","Novel, Animals","6000");
insert into Books values(8,"Inheritance Of Loss","Kiran"," PhysOics","3000");
insert into Books values(9,"The Electric Acid Test","Tom Wolfe","Novel,Electric","5000");
insert into Books values(10,"A Suitable Boy","Venkat ","novel","5200");


create table LoginUser(
id int(10) auto_increment primary key,
name varchar(20),
phone varchar(10),
email varchar(30),
password varchar(10),
country varchar(10));

select * from LoginUser;

create table Favorites(
id int(10) auto_increment primary key,
Title varchar(50),
Authorname varchar(30),
Genre varchar(30),
Price float);

select * from Favorites;

create table ReadLater(
id int(20) auto_increment primary key,
Title varchar(60),
Authorname varchar(50),
Genre varchar(60),
Price float);

select * from ReadLater;

create table favorit(
rid int auto_increment primary key,
id int(30),
foreign key(id) references books(id)
);

select * from favorit;

create table readlate(
rid int auto_increment primary key,
id int(20),
foreign key(id) references books(id)
);

select * from readlate;

