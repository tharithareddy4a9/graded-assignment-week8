package com.hcl.library;

	import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

	import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

	public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

		public AppInitializer() {
			System.out.println("App initalizer loaded");
		}

		@Override
		protected Class<?>[] getRootConfigClasses() {
			return null;
		}

		@Override
		protected Class<?>[] getServletConfigClasses() {
			System.out.println("App initializer");
			return new Class<?>[] { WebAppConfig.class };
		}

		@Override
		protected String[] getServletMappings() {
			return new String[] { "/" };
		}

	}


