
	package com.hcl.library;

	import javax.sql.DataSource;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.ComponentScan;
	import org.springframework.context.annotation.Configuration;
	import org.springframework.jdbc.core.JdbcTemplate;
	import org.springframework.jdbc.datasource.DriverManagerDataSource;
	import org.springframework.web.servlet.ViewResolver;
	import org.springframework.web.servlet.config.annotation.EnableWebMvc;
	import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
	import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
	import org.springframework.web.servlet.view.InternalResourceViewResolver;
	import org.springframework.web.servlet.view.JstlView;

	@Configuration
	@EnableWebMvc
	@ComponentScan
	public class WebAppConfig implements WebMvcConfigurer {
		@Bean
		public ViewResolver viewResolver() {
			System.out.println("*VIEW RESOLVER*");
			InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
			viewResolver.setViewClass(JstlView.class);
			viewResolver.setPrefix("/WEB-INF/pages/");
			viewResolver.setSuffix(".jsp");
			return viewResolver;
		}

			public void addResourceHandlers(ResourceHandlerRegistry registry) {
			registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
		}

		@Bean
		public DataSource connect() {
			DriverManagerDataSource ds = new DriverManagerDataSource();
			ds.setUrl("jdbc:mysql://localhost:3306/library");
			ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
			ds.setUsername("root");
			ds.setPassword("Haritha@456");
			System.out.println("DATABASE CONNECTED.....!");
			return ds;
		}

		@Bean
		@Autowired
		public JdbcTemplate template(DataSource datasource) {
			JdbcTemplate template = new JdbcTemplate(datasource);
			System.out.println(template);
			return template;
		}

	}


