
package com.hcl.library.entity;

public class Books {

	private int id;
	private String title;
	private String authorName;
	private String genre;
	private double price;

	public Books() {

	}

	public Books(int id, String title, String authorName, String genre, double price) {
		super();
		this.id = id;
		this.title = title;
		this.authorName = authorName;
		this.genre = genre;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Books [id=" + id + ", title=" + title + ", authorName=" + authorName + ", genre=" + genre + ", price="
				+ price + "]";
	}

}
