package com.hcl.library.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.library.entity.Books;
import com.hcl.library.entity.LoginToBooks;

@Repository
public class BookRepository {

	@Autowired
	private JdbcTemplate template;

	public boolean loginUser(LoginToBooks user) {
		System.out.println("-------------------" + user);
		String sql1 = "select email,password from LoginUser";
		Map<String, String> userlist = new HashMap<String, String>();
		boolean value = false;
		List<LoginToBooks> validate = template.query(sql1, new RowMapper<LoginToBooks>() {

			@Override
			public LoginToBooks mapRow(ResultSet rs1, int rowNum) throws SQLException {
				LoginToBooks login = new LoginToBooks();
				String email = login.setEmail(rs1.getString(1));
				String password = login.setPassword(rs1.getString(2));
				userlist.put(email, password);
				return login;
			}
		});
		for (Map.Entry<String, String> event : userlist.entrySet()) {
			if (user.getEmail().equals(event.getKey()) & user.getPassword().equals(event.getValue())) {
				value = true;
				break;
			} else {
				value = false;
			}
		}
		return value;
	}

	public boolean registerData(LoginToBooks user) {
		try {
			String sql = "insert into LoginUser(name,country,email,phone,password) values(?,?,?,?,?)";
			template.update(sql, new Object[] { user.getName(), user.getCountry(), user.getEmail(), user.getPhone(),
					user.getPassword() });
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	public List<Books> getAllBooks() {
		String sql = "Select * from Books";
		List<Books> listBooks = template.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books Book = new Books();
				Book.setId(rs.getInt(1));
				Book.setTitle(rs.getString(2));
				Book.setAuthorName(rs.getString(3));
				Book.setGenre(rs.getString(4));
				Book.setPrice(rs.getDouble(5));
				System.out.println(Book);
				return Book;
			}
		});
		return listBooks;
	}

	public boolean inserter(int id) {
		String sqls = "INSERT INTO favorit(id) VALUES(?)";
		try {

			template.update(sqls, new Object[] { id });

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public List<Books> getFavBooks() {
		String sql = "select f.rid,f.id,b.Title,b.authorname,b.genre,b.Price from favorit f left join books b on f.id=b.id;";
		List<Books> listBooks = template.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books book = new Books();
				book.setId(rs.getInt(2));
				book.setTitle(rs.getString(3));
				book.setAuthorName(rs.getString(4));
				book.setGenre(rs.getString(5));
				book.setPrice(rs.getDouble(6));

				return book;
			}
		});
		return listBooks;
	}

	public boolean insert(int id) {
		String sqls = "INSERT INTO readlate(id) VALUES(?)";
		try {

			template.update(sqls, new Object[] { id });

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public List<Books> getreadlateBooks() {
		String sql = "select f.rid,f.id,b.Title,b.authorname,b.genre,b.Price from readlate f left join books b on f.id=b.id;";
		List<Books> listBooks = template.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs1, int rowNum) throws SQLException {
				Books book = new Books();
				book.setId(rs1.getInt(2));
				book.setTitle(rs1.getString(3));
				book.setAuthorName(rs1.getString(4));
				book.setGenre(rs1.getString(5));
				book.setPrice(rs1.getDouble(6));

				return book;
			}
		});
		return listBooks;
	}

	public List<Books> getVisitBooks() {
		String sql = "Select * from Books";
		List<Books> listBooks = template.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books Book = new Books();
				Book.setId(rs.getInt(1));
				Book.setTitle(rs.getString(2));
				Book.setAuthorName(rs.getString(3));
				Book.setGenre(rs.getString(4));
				Book.setPrice(rs.getDouble(5));
				System.out.println(Book);
				return Book;
			}
		});
		return listBooks;
	}

}