package com.hcl.mapping;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.mapping.entities.Cart;
import com.hcl.mapping.entities.Item;
import com.hcl.mapping.repository.CartRepository;
import com.hcl.mapping.repository.ItemRepository;

@SpringBootApplication
public class ManyToManyApplication implements CommandLineRunner {

    @Autowired
    private ItemRepository itemRepo;

    @Autowired
    private CartRepository cartRepo;
	

	
	public static void main(String[] args) {
		SpringApplication.run(ManyToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		
		Cart cart1 = new Cart();

        Item item1 = new Item();
        item1.setDescription("Saree");
        item1.setPrice(5000);
        
        Item item2 = new Item();
        item2.setDescription("t-shirt");
        item2.setPrice(1600);


        Set<Item> itemset = new HashSet<Item>();
       
        itemset.add(item1);
        itemset.add(item2);
        
         cart1.setCarts(itemset);
         cart1.setLastUpdatedAt(new Date(2022-01-29));
         cart1.setCreatedAt(new Date(2022-01-29));    
         
         cartRepo.save(cart1);
         
     	
 		Cart cart2 = new Cart();

         Item itemCart1 = new Item();
         itemCart1.setDescription("Kurti");
         itemCart1.setPrice(2000);
         
         Item itemCart2 = new Item();
         itemCart2.setDescription("shirt");
         itemCart2.setPrice(1600);


         Set<Item> itemset1 = new HashSet<Item>();
        
         itemset1.add(itemCart1);
         itemset1.add(itemCart2);
         
          cart2.setCarts(itemset1);
          cart2.setLastUpdatedAt(new Date(2022-01-29));
          cart2.setCreatedAt(new Date(2022-01-29));  
          
          cartRepo.save(cart2);

		
	}

}
