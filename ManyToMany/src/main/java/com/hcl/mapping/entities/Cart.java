package com.hcl.mapping.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Cart {

	@Id
	@GeneratedValue
	private Long id;

	private Date createdAt ;
	
	private Date lastUpdatedAt;

	@ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "cart_items")

    private Set carts = new HashSet<>();

	public Cart() {
		super();
	}
	public Cart(Long id, Long cart_total, Date createdAt, Date lastUpdatedAt, Set carts) {
		super();
		this.id = id;
		this.createdAt = createdAt;
		this.lastUpdatedAt = lastUpdatedAt;
		this.carts = carts;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
		public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}
	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}
	public Set getCarts() {
		return carts;
	}
	public void setCarts(Set carts) {
		this.carts = carts;
	}
	@Override
	public String toString() {
		return "Cart [id=" + id + ", createdAt=" + createdAt + ", lastUpdatedAt="
				+ lastUpdatedAt + ", carts=" + carts + "]";
	}
	

	
	
}
