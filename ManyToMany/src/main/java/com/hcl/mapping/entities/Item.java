package com.hcl.mapping.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "cart")
@Entity
public class Item {

	@Id
    @GeneratedValue
    private Long id;
	
    private String description;
    private long price;
    
    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "items")

    private Set items = new HashSet<>();

    public Item() {
    	super();
    }
	public Item(Long id, String description, long price, Set items) {
		super();
		this.id = id;
		this.description = description;
		this.price = price;
		this.items = items;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public Set getItems() {
		return items;
	}

	public void setItems(Set items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Item [id=" + id + ", description=" + description + ", price=" + price + ", items=" + items + "]";
	}

    
}
