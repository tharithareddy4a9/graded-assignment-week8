package com.hcl.mapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.mapping.entities.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item,Long>{

}
