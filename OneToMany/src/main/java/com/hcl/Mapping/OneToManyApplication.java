package com.hcl.Mapping;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.Mapping.entities.Book;
import com.hcl.Mapping.entities.Story;
import com.hcl.Mapping.repository.BookRepository;
import com.hcl.Mapping.repository.StoryRepository;

@SpringBootApplication
public class OneToManyApplication implements CommandLineRunner{

	@Autowired
	private BookRepository bookRepo;
	
	@Autowired
	private StoryRepository storyRepo;
	
	
	public static void main(String[] args) {
		SpringApplication.run(OneToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		 Book book =new Book();
			
			book.setBookName("WingsOfFire");
		    book.setBookAuthor("APJAbdulKalam");
			
			
			Set<Story> set = new HashSet<Story>();
			
			Story story1= new Story();
			story1.setStoryName("HappyPrince");
			

			Story story2= new Story();
			story2.setStoryName("MagicShop");
			

			Story story3= new Story();
			story3.setStoryName("Monsters");
			

			Story story4= new Story();
			story4.setStoryName("TheEnd");
			
		
	      set.add(story1);
	      set.add(story2);
	      set.add(story3);
	      set.add(story4);
	      
	      storyRepo.save(story1);
	      storyRepo.save(story2);
	      storyRepo.save(story3);
	      storyRepo.save(story4);
		
	       book.setStory(set);
	      
	        bookRepo.save(book); 
			
			
		/*	Story story= storyRepo.findById(9).orElse(null);
			
			System.out.println(story); 
			
		  	Story story11 = new Story();
			story11.setStoryName("TheBox");
			
		    Book book11 = new Book();
		
	       	book11.setBookId(10);
		
	    	story11.setBook(book11);
		
	        storyRepo.save(story11);  */ 
			
		
	}

}
