package com.hcl.Mapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.Mapping.entities.Story;

@Repository
public interface StoryRepository extends JpaRepository<Story ,Integer> {

}
