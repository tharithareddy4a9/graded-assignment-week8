package com.hcl.Mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.Mapping.entities.Address;
import com.hcl.Mapping.entities.Person;
import com.hcl.Mapping.repositories.AddressRepository;
import com.hcl.Mapping.repositories.PersonRepository;



@SpringBootApplication
public class OneToOneApplication  implements CommandLineRunner {

	@Autowired
	private AddressRepository addressRepo;
	
	@Autowired
	private PersonRepository personRepo;

	public static void main(String[] args) {
		SpringApplication.run(OneToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Person person = new Person();
		
		person.setName("Haritha");
		person.setEmail("Haritha@gmail.com");
		person.setPassword("Hari456");
		
		Address address = new Address();
		
		address.setStreet("saiStreet");
		address.setCity("Kurnool");
		address.setState("AndhraPradesh");
		address.setPinCode(521406);
		address.setCountry("India");
		

		addressRepo.save(address);
		
	    person.setAddress(address);
		
		personRepo.save(person);
		
		
      //  Person person1 = personRepo.findById((long) 15).orElse(null);
		
	 //   System.out.println(person1);
		
	//	Address address1 =	addressRepo.findById(14).orElse(null);
		
	//	System.out.println(address1);

	}

}
