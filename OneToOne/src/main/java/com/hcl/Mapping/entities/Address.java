package com.hcl.Mapping.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Address {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private Long id;
	    private String street;
	    private String city;
	    private String state;
	    private int pinCode;
	    private String country;
	    @OneToOne(mappedBy = "address",fetch = FetchType.EAGER)
	    private Person person;
	    
	    public Address(){
	    	super();
	    }
		public Address(Long id, String street, String city, String state, int pinCode, String country, Person person) {
			super();
			this.id = id;
			this.street = street;
			this.city = city;
			this.state = state;
			this.pinCode = pinCode;
			this.country = country;
			this.person = person;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getStreet() {
			return street;
		}
		public void setStreet(String street) {
			this.street = street;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public int getPinCode() {
			return pinCode;
		}
		public void setPinCode(int pinCode) {
			this.pinCode = pinCode;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public Person getPerson() {
			return person;
		}
		public void setPerson(Person person) {
			this.person = person;
		}
		@Override
		public String toString() {
			return "Address [id=" + id + ", street=" + street + ", city=" + city + ", state=" + state + ", pinCode="
					+ pinCode + ", country=" + country + ", person=" + person + "]";
		}

		
	    
}
