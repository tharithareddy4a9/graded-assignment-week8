package com.hcl.Mapping.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.Mapping.entities.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address ,Long> {

}
