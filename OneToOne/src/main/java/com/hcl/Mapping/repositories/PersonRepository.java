package com.hcl.Mapping.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.Mapping.entities.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person,Long>{

}
