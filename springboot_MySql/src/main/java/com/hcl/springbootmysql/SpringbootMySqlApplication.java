package com.hcl.springbootmysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.springbootmysql.repository.CourseRepository;

@SpringBootApplication
public class SpringbootMySqlApplication {

	
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootMySqlApplication.class, args);
		
		
		
	}

}
