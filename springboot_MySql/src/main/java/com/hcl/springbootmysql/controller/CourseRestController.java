package com.hcl.springbootmysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbootmysql.entity.Course;
import com.hcl.springbootmysql.service.ICourseService;


@RestController
@RequestMapping("Course/v1")

public class CourseRestController {




@Autowired
ICourseService service;

@PostMapping(value = "/add",consumes="application/json")
public Course insert (@RequestBody Course student) {
	return service.addCourse(student);
}

@GetMapping("/getall")
public List<Course>  getAll(){
	
	return service.getAllCourse();
}

@PutMapping(value="/update" , consumes="application/json")
public Course  update(@RequestBody  Course  course) {
	
	return   service.updateCourse(course);
	
}


@DeleteMapping("/remove/{id}")
public ResponseEntity<String > remove(@PathVariable int id) {
	
return service.deleteCourseById(id);

}


@GetMapping("/get/{id}")
public  Course  getCourseById(@PathVariable int id) {
	
	return  service.getCoursetById(id);
	
}

}
