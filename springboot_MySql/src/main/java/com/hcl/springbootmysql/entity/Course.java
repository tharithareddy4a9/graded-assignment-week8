package com.hcl.springbootmysql.entity;




import java.sql.Date;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EntityScan

public class Course {

	//@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	//@Column(name="student_name")
	private String name;
	private double price;
	private int Credits;
	private Date Duration;
	
}
