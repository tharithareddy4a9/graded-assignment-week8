package com.hcl.springbootmysql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.springbootmysql.entity.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course,Integer>{

	Course save(Course course);

	List<Course> findAll();

	void deleteById(int id);

	Course findById(int id);

	


}
