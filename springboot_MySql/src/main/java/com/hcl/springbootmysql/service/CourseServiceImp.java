package com.hcl.springbootmysql.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.springbootmysql.entity.Course;
import com.hcl.springbootmysql.repository.CourseRepository;

class CourseServiceImp implements ICourseService {


	@Autowired
	CourseRepository repo;

	
	
	@Override
	public Course addCourse(Course course) {
		// TODO Auto-generated method stub
		return repo.save(course);
	}

	@Override
	public List<Course> getAllCourse() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public Course updateCourse(Course course) {
		// TODO Auto-generated method stub
		return  repo.save(course);
	}

	@Override
	public ResponseEntity<String> deleteCourseById(int id) {
		// TODO Auto-generated method stub
		 repo.deleteById(id);
		return new ResponseEntity<String>("Record Deleted",HttpStatus.ACCEPTED);
	}

	@Override
	public Course getCoursetById(int id) {
		// TODO Auto-generated method stub
		return repo.findById(id);
	}


	
	
}