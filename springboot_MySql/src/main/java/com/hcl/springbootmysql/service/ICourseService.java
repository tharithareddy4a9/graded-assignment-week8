package com.hcl.springbootmysql.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.hcl.springbootmysql.entity.Course;

public interface ICourseService {
	public Course addCourse(Course course);
	
	public List<Course> getAllCourse();

	public Course updateCourse(Course course);

    public ResponseEntity<String> deleteCourseById(int id);

	public Course getCoursetById(int id);


}
